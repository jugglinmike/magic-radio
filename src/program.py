from datetime import datetime, timedelta
import math
import os
import os.path

from errors import InvalidConfigurationError
from utils.file_duration import file_duration as read_file_duration
from playlist import Playlist

def find_all_audio_files(directory):
    all_filenames = set()

    for dirpath, dirnames, dirfilenames in os.walk(directory):
        for dirfilename in dirfilenames:
            if dirfilename == 'magic-radio-history.txt':
                continue
            all_filenames.add(os.path.join(dirpath, dirfilename))

    return all_filenames

class Program(object):
    def __init__(self, start, duration, audio):
        self.start = start
        self.duration = duration
        self.audio = audio

    @property
    def is_selection_based(self):
        return self.duration[1] == 'selection'

    @property
    def uses_directory(self):
        return os.path.isdir(self.audio)

    @property
    def uses_history(self):
        return self.is_selection_based and self.uses_directory

    def most_recent_start_time(self, at_time):
        most_recent_start_time = datetime(1970, 1, 1)

        for start_item in self.start:
            a_recent_start_time = _get_most_recent_start_time(at_time, start_item)

            if a_recent_start_time > at_time:
                continue
            elif a_recent_start_time < most_recent_start_time:
                continue

            most_recent_start_time = a_recent_start_time

        return most_recent_start_time

    def playlist(self, at_time, memoize):
        all_filenames = set()
        filenames = None
        duration = None
        file_durations = None
        offset = None
        history = None
        shuffle = None

        if self.uses_directory:
            all_filenames = memoize(find_all_audio_files)(self.audio)
        else:
            all_filenames = set([self.audio])

        most_recent_start_time = self.most_recent_start_time(at_time)

        if self.is_selection_based:
            shuffle = False
            history = self.read_history(most_recent_start_time)
            unplayed_filenames = all_filenames - history

            # If every file has been played, clear the history and proceed as
            # though none of the files have been played before.
            if len(unplayed_filenames) == 0:
                unplayed_filenames = all_filenames
                os.remove(self.history_filename)

            filenames = sorted(unplayed_filenames)[:self.duration[0]]

            memoized_read_file_duration = memoize(read_file_duration)
            file_durations = list(map(
                lambda filename: memoized_read_file_duration(filename) / 1000 / 60,
                filenames[:self.duration[0]]
            ))
            duration = sum(file_durations)
        else:
            shuffle = True
            filenames = sorted(all_filenames)
            duration = self.duration[0]

        most_recent_end_time = most_recent_start_time + timedelta(0, 60 * duration)

        if most_recent_start_time > at_time or most_recent_end_time <= at_time:
            return

        if self.is_selection_based:
            offset = math.floor(
                (at_time - most_recent_start_time).total_seconds() / 60
            )
            for file_duration in file_durations:
                if offset >= file_duration:
                    offset -= round(file_duration)
                    filenames.pop(0)
                else:
                    break

        return Playlist(tuple(filenames), offset, shuffle)

    @property
    def history_filename(self):
        return os.path.join(self.audio, 'magic-radio-history.txt')

    def read_history(self, most_recent_start_time):
        history = set()

        if not self.uses_history:
            return history

        try:
            with open(self.history_filename, 'r') as handle:
                contents = handle.read()
        except FileNotFoundError:
            return history

        parts = None

        for line in contents.split('\n'):
            parts = line.split(maxsplit=1);
            if len(parts) < 2:
                continue

            # The history is extended at the moment a file begins playing. This
            # does not mean that the file completed, though, so any entries
            # describing playback for the "live" program instance should be
            # ignored (such files will be excluded from the playlist according
            # to their duration and the most recent start time).
            if parts[0] == most_recent_start_time.isoformat():
                continue
            history.add(os.path.join(self.audio, parts[1]))

        return history

    def write_history(self, at_time, playlist):
        if not self.uses_history:
            return

        timestamp = self.most_recent_start_time(at_time).isoformat()
        new_lines = []

        for filename in playlist.filenames:
            relative_filename = os.path.relpath(filename, self.audio)
            new_lines.append('{} {}'.format(timestamp, relative_filename))

        with open(self.history_filename, 'a+') as handle:
            handle.write('\n'.join(new_lines) + '\n')

def _get_most_recent_start_time(at_time, start_item):
    repetition = start_item.repetition
    day = start_item.day
    time = start_item.time

    if repetition == 'once':
        most_recent_start_time = datetime(
            day[0], day[1], day[2], time[0], time[1]
        )
    elif repetition == 'weekly':
        # Week days in this project's configuration file are Sunday-indexed,
        # but datetime's week days are Monday-indexed.
        current_weekday = (at_time.weekday() + 1) % 7

        most_recent_start_time = datetime(
            at_time.year, at_time.month, at_time.day, time[0], time[1]
        ) - timedelta((7 - (day - current_weekday)) % 7)

        # This will be true whenever the program is scheduled for the
        # current day but after the current time (i.e. "later today")
        if most_recent_start_time > at_time:
            most_recent_start_time -= timedelta(7)
    elif repetition == 'yearly':
        most_recent_start_time = datetime(
            at_time.year, day[0], day[1], time[0], time[1]
        )
        if most_recent_start_time > at_time:
            most_recent_start_time -= timedelta(365)

    return most_recent_start_time
