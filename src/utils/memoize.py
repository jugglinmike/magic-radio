from base64 import urlsafe_b64encode
import os
import os.path
import pickle
import shutil

def null_memoizer(fn):
    return fn

def create_memoizer(cache_dir=None):
    '''Create a memoizer: a factory function which generates memoized versions
    of input functions. When the memoized version is invoked, it only calls
    through to the true implementation if the result has not previously been
    calculated and stored in a file in the `cache_dir`. The signature of the
    input function must be a single string parameter.

    This abstraction is available for operations that interact with the disk.
    Because this application is expected to be run on a frequent schedule,
    memoization reduces wear on the system's storage device.
    '''
    if cache_dir == None:
        return null_memoizer

    def memoize(fn):
        cache_subdirectory = os.path.join(cache_dir, fn.__name__)
        cached_filename_format = os.path.join(cache_subdirectory, '{}.json')

        def memoized(filename):
            cached = cached_filename_format.format(
                urlsafe_b64encode(filename.encode('utf-8')).decode('utf-8')
            )

            try:
                with open(cached, 'br') as handle:
                    return pickle.loads(handle.read())
            except FileNotFoundError:
                pass

            result = fn(filename)

            def save():
                with open(cached, 'bw') as handle:
                    handle.write(pickle.dumps(result))
            try:
                save()
            except FileNotFoundError:
                # Because this program is run according to a schedule and in
                # response to end-user interaction (e.g. the insertion of a
                # removable storage device), it's possible that a directory
                # which was absent during the initial attempt to save has since
                # been created by another process. `exist_ok` is used to
                # support this condition.
                os.makedirs(cache_subdirectory, exist_ok=True)
                save()

            return result

        return memoized

    return memoize

def clear_cache(cache_dir=None):
    if cache_dir is None:
        return

    for root, dirs, files in os.walk(cache_dir):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
