import re
import subprocess

def file_duration(file_name):
    '''Retrieve the duration of an audio file in milliseconds.

    This function calculated duration by decoding the audio file. Many audio
    files include metadata which describes the duration, and although reading
    metadata is significantly faster than decoding audio, it is also
    potentially inaccurate.
    '''
    args=('ffmpeg', '-i', file_name, '-f', 'null', '-')
    child = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # ffmpeg may output a large amount of data to the standard error
    # stream--enough to fill the OS pipe buffer. If the child's buffer is
    # filled, the parent program will deadlock. Avoid this by reading output
    # one line at a time and discarding all but the final lines (earlier lines
    # will not describe the file's duration).
    lines = []
    for line in child.stdout:
        lines.append(line)
        if len(lines) > 5:
            lines.pop(0)

    child.communicate()
    if child.returncode != 0:
        with open(file_name, 'r'):
            pass
        raise TypeError

    output = '\n'.join(map(lambda line: line.decode('utf8'), lines))
    matches = re.findall(r'\btime=([0-9]+):([0-9]+):([0-9]+\.[0-9]+)', str(output))
    return round(1000 * sum([
        int(matches[-1][0]) * 60 * 60,
        int(matches[-1][1]) * 60,
        float(matches[-1][2])
    ]))
