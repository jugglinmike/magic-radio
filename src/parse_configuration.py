from collections import namedtuple
import re
import os
import os.path
from datetime import datetime
import yaml

import errors
from program import Program

weekdays = (
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
)

Configuration = namedtuple('Configuration', 'version frequency programming')
StartItem = namedtuple('StartItem', 'day time repetition')

def parse_configuration(dirname, source):
    raw = yaml.safe_load(source)

    if 'version' not in raw:
        raise errors.MissingConfigurationError('version')
    version = raw['version']

    if 'frequency' not in raw:
        raise errors.MissingConfigurationError('frequency')

    frequency = _parse_frequence(raw['frequency'])

    if 'programming' not in raw:
        raise errors.MissingConfigurationError('programming')

    programming = []

    for raw_program in raw['programming']:
        if 'start' not in raw_program:
            raise errors.MissingConfigurationError('start')

        start = _parse_start(raw_program['start'])

        if 'duration' not in raw_program:
            raise errors.MissingConfigurationError('duration')

        duration = _parse_duration(raw_program['duration'])

        if 'audio' not in raw_program:
            raise errors.MissingConfigurationError('duration')

        audio = _parse_audio(dirname, raw_program['audio'])

        programming.append(Program(start, duration, audio))

    return Configuration(
        version=version, frequency=frequency, programming=programming
    )

def _parse_frequence(raw_frequency):
    if raw_frequency == None:
        return None

    if type(raw_frequency) == str:
        match = re.search(
            r'([1-9][0-9]{,2}(\.[0-9])?)(?:\s*mhz)?',
            raw_frequency,
            re.IGNORECASE
        )
        frequency = float(match[1])
    else:
        frequency = raw_frequency

    if type(frequency) not in (int, float):
        raise errors.InvalidConfigurationError('frequency', frequency)

    if frequency < 87.5 or frequency > 108:
        raise errors.InvalidConfigurationError('frequency', frequency)

    return int(frequency * 1000)

def _parse_start(start):
    if type(start) == str:
        start = [start]

    parsed = []

    for start_item in start:
        parsed.append(_parse_start_item(start_item))

    return parsed

def _parse_start_item(start_item):
    parts = start_item.split(" at ")
    if len(parts) != 2:
        raise errors.InvalidConfigurationError('start', start_item)

    raw_day = parts[0].lower()
    raw_time = parts[1]
    day = None
    repetition = None

    try:
        date = datetime.strptime(raw_day, '%Y-%m-%d')
        day = (date.year, date.month, date.day)
        repetition = 'once'
    except ValueError:
        pass

    if day is None:
        try:
            date = datetime.strptime(raw_day, '%B %d')
            day = (date.month, date.day)
            repetition = 'yearly'
        except ValueError:
            pass

    if day is None:
        # https://bugs.python.org/issue26460
        if raw_day == 'february 29':
            day = (2, 29)
            repetition = 'yearly'
        elif raw_day in weekdays:
            day = weekdays.index(raw_day)
            repetition = 'weekly'
        elif raw_day[-1] == 's' and raw_day[:-1] in weekdays:
            day = weekdays.index(raw_day[:-1])
            repetition = 'weekly'

    if day is None:
        raise errors.InvalidConfigurationError('start', start_item)

    try:
        parsed_time = datetime.strptime(raw_time, '%I:%M %p')
    except ValueError:
        raise errors.InvalidConfigurationError('start', start_item)

    time = (parsed_time.hour, parsed_time.minute)

    return StartItem(day=day, time=time, repetition=repetition)

def _parse_duration(duration):
    match = re.search(r'([1-9]\d*) (selection|minute|hour)s?', duration)

    if match is None:
        raise errors.InvalidConfigurationError('duration', duration)

    number = int(match[1])
    type = match[2]

    if type == 'hour':
        number *= 60
        type = 'minute'

    return (number, type)

def _parse_audio(dirname, audio):
    full_path = os.path.join(dirname, audio)

    if not os.path.exists(full_path):
        raise errors.InvalidConfigurationError('audio', audio)

    if os.path.isdir(full_path):
        for _, __, files in os.walk(full_path):
            if len(files):
                break
        else:
            raise errors.InvalidConfigurationError('audio', audio)

    return full_path
