from collections import namedtuple

Playlist = namedtuple('Playlist', 'filenames offset shuffle')
