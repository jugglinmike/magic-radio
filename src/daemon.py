from contextlib import contextmanager
import math
import os.path

from mpd import MPDClient

from playlist import Playlist

@contextmanager
def connect(directory, host, port):
    client = MPDClient()
    # network timeout in seconds (floats allowed), default: None
    client.timeout = 10
    # timeout for fetching the result of the idle command is handled
    # seperately, default: None
    client.idletimeout = None
    client.connect(host, port)
    daemon = Daemon(directory, client)

    yield daemon

    # send the close command
    client.close()
    # disconnect from the server
    client.disconnect()

class Daemon(object):
    def __init__(self, directory, client):
        self._client = client
        self._directory = directory

    def rescan(self):
        self._client.rescan()

    def get_playlist(self):
        status = self._client.status()

        # oddly, a string
        shuffle = status['random'] == '1'

        if status['state'] == 'play':
            filenames = tuple(
                os.path.join(self._directory, item.replace('file: ', ''))
                for item in self._client.playlist()
            )
            offset = math.floor(float(status['elapsed']) / 60)
        else:
            filenames = ()
            offset = 0

        return Playlist(filenames=filenames, offset=offset, shuffle=shuffle)

    def stop(self):
        self._client.clear()
        self._client.stop()

    def play(self, playlist):
        self._client.clear()

        for filename in playlist.filenames:
            self._client.add(os.path.relpath(filename, self._directory))

        self._client.random(int(playlist.shuffle))

        self._client.play()

        if playlist.offset:
            self._client.seekcur(playlist.offset * 60)
