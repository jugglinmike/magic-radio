import board
import busio
import digitalio
import adafruit_si4713

def set_frequency(frequency):
    i2c = busio.I2C(board.SCL, board.SDA)
    si_reset = digitalio.DigitalInOut(board.D5)
    si4713 = adafruit_si4713.SI4713(i2c, reset=si_reset, timeout_s=0.5)

    # The FM transmitter device has been observed to ignore the initial attempt
    # to modify frequency following startup. Subsequent modifications are
    # applied as expected.
    #
    # The following condition accommodates that behavior. If the device reports
    # a frequency of `0`, interpret this to mean that the current invocation is
    # the first since its coming online (although it will also be `0` following
    # manual reset) and issue the signal twice in order.
    if si4713.tx_frequency_khz == 0:
        si4713.tx_frequency_khz = frequency

    si4713.tx_frequency_khz = frequency
    si4713.tx_power = 115
