class ConfigurationError(ValueError):
    pass

class MissingConfigurationError(ConfigurationError):
    def __init__(self, key):
        super(MissingConfigurationError, self).__init__('missing key {}'.format(key))

class InvalidConfigurationError(ConfigurationError):
    def __init__(self, key, value):
        message = 'invalid value for {}: "{}"'.format(key, value)
        super(InvalidConfigurationError, self).__init__(message)
