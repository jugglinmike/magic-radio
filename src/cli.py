import argparse
from datetime import datetime
import os

def validate_directory(value):
    if not os.path.isdir(value):
        raise ValueError('"{}" is not a directory'.format(value))

    return value

def validate_address(value):
    parts = value.split(':')
    if len(parts) != 2 or not parts[0] or int(parts[1]) < 0:
        raise ValueError('"{}" is not a network address'.format(value))

    return value

def parse_args():
    parser = argparse.ArgumentParser(description='Play audio according to programming')
    parser.add_argument('directory')
    parser.add_argument(
        '--rescan',
        action='store_true',
        help='''Refresh the audio file database and destroy any cached data
        previously created with the `--cache-dir` option. This option should be
        used whenever the configuration file changes, audio files are added, or
        audio files are removed.'''
    )
    parser.add_argument(
        '--at-time',
        default=datetime.now(),
        type=lambda value: datetime.fromisoformat(value),
        help='''Behave as though invoked at the moment described by an input
        time specifier. This value should follow the format of Python's
        `isoformat` function:
        https://docs.python.org/3/library/datetime.html#datetime.datetime.isoformat'''
    )
    parser.add_argument(
        '--verbose',
        action='store_true',
        help='Write extra debugging information to standard out'
    )
    parser.add_argument(
        '--cache-dir',
        type=validate_directory,
        default=None,
        help='''Save the result of expensive operations to a location on the
        file system, and reference those stored values when available. This is
        intended to reduce wear on physical storage devices, so if specified,
        it should be to a directory on an in-memory file system. The cached
        values will be cleared if the `--rescan` option is specified.'''
    )
    parser.add_argument(
        '--mpd-address',
        type=validate_address,
        default='localhost:6600',
        help='''The network address on which Music Player Daemon is
        listening'''
    )
    return dict(vars(parser.parse_args()))
