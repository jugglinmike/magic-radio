import logging
import os.path
import re
import sys

from utils.memoize import create_memoizer, clear_cache
from parse_configuration import parse_configuration
from daemon import connect

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def load_configuration(directory):
    config_filename = os.path.join(directory, 'magic-radio.txt')
    with open(config_filename, 'r') as handle:
        return parse_configuration(directory, handle.read())

def write_instructions(directory):
    readme_filename = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        '..',
        'README.md'
    )
    instructions_pattern = (
        r'<!--BEGIN_INJECTED_INSTRUCTIONS-->'
        r'(.*)'
        r'<!--END_INJECTED_INSTRUCTIONS-->'
    )
    instructions_filename = os.path.join(
        directory, 'magic-radio-instructions.txt'
    )

    with open(readme_filename, 'r') as handle:
        instructions_content = re.search(
            instructions_pattern, handle.read(), re.DOTALL
        ).group(1).strip()

    with open(instructions_filename, 'w') as handle:
        handle.write(instructions_content + '\n')

def main(at_time, directory, rescan, verbose, cache_dir, mpd_address):
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    memoize = create_memoizer(cache_dir)

    if rescan:
        logger.info('Clearing cached data')
        clear_cache(cache_dir)
        logger.info('Writing usage instructions to storage device')
        write_instructions(directory)

    configuration = memoize(load_configuration)(directory)

    if rescan:
        if configuration.frequency:
            # The `fm` module requires the FM transmitter hardware. It will
            # raise an exception if imported in the absence of that hardware.
            # Defer importing in order to support applications which do not
            # include an FM transmitter.
            import fm
            logger.info(
                'Broadcasting on {:0.1f}Mhz'.format(configuration.frequency/1000)
            )
            fm.set_frequency(configuration.frequency)
        else:
            logger.info('No frequency specified. Not broadcasting.')

    host, port = mpd_address.split(':')

    with connect(directory, host, int(port)) as daemon:
        if rescan:
            logger.info('Rescanning audio files')
            daemon.rescan()

        current_playlist = daemon.get_playlist()

    for program in configuration.programming:
        logger.debug('Considering program "%s"', program.audio)
        desired_playlist = program.playlist(at_time, memoize)

        if desired_playlist:
            logger.debug(
                'Found program with active playlist: "%s"', program.audio
            )
            break
    else:
        logger.debug('No programs have an active playlist.')

    with connect(directory, host, int(port)) as daemon:
        def play():
            logger.info(
                'Starting program "%s" (playlist size: %s and offset: %s)',
                program.audio,
                len(desired_playlist.filenames),
                desired_playlist.offset,
            )
            daemon.play(desired_playlist)
            program.write_history(at_time, desired_playlist)

        if not desired_playlist:
            if current_playlist.filenames:
                logger.info('Stopping program')
                daemon.stop()
            return
        elif not current_playlist.filenames:
            if desired_playlist:
                play()
            return

        union = set(current_playlist.filenames) & set(desired_playlist.filenames)
        if len(union):
            logger.debug(
                'Current playlist has overlap with desired playlist. No action necessary.'
            )
            return

        play()

if __name__ == '__main__':
    from errors import ConfigurationError
    from cli import parse_args

    try:
        main(**parse_args())
    except:
        logger.error('Uncaught exception:', exc_info=True)
        sys.exit(1)
