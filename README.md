# Magic Radio

This project implements a [free-as-in-freedom automated music
player](http://www.gnu.org/philosophy/free-sw.html). Once installed, you can
control it by writing a text-based configuration file that defines an audio
programming schedule, along with a set of audio files.

The project is intended to be installed on [a Raspberry Pi model 4
computer](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/), and
instructions for doing this are included below. Folks with some experience in
[Python](https://www.python.org/) and GNU/Linux can use parts of this for other
systems, but (beyond some suggestions listed below) this repository doesn't
include any specific guidance for alternate configurations.

<!--
  The application code copies the content between the following set of comments
  to the end user's storage device.
-->
<!--BEGIN_INJECTED_INSTRUCTIONS-->

## How to define audio programming

The Magic Radio uses a text file to determine when to play audio files. It's
named `magic-radio.txt`, and it has to be placed in the "top level" (also
called the "root") of the storage device.

The file can be modified with any text editor; just take care to save it as
text (some text editors may insert invisible characters when saving other types
of files).

The file uses a special syntax called
[YAML](https://en.wikipedia.org/wiki/YAML). Characters like colons and hyphens
have specific meaning, as does indentation. You can learn all about the rules
of YAML, but it's designed to be familiar for people write in Western
languages, so you might find it easier to learn from an example.

Here's an example configuration:

```
frequency: 101.7 Mhz
programming:
  - audio: talk/Gunsmoke
    start: Wednesday at 18:30
    duration: 2 selections
  - audio: 90s-rock
    start:
    - Monday at 8:00 PM
    - Tuesday at 8:00 PM
    - Wednesday at 8:00 PM
    - Thursday at 8:00 PM
    - Friday at 8:00 PM
    duration: 1 hour
  - audio: singles/Everlast - Black Coffee.mp3
    start: April 25 at 4:00 PM
    duration: 1 selection
```

Fortunately, the YAML syntax allows us to explain the configuration with notes
written directly into the file. Lines that begin with the number sign (`#`) are
known as "comments." The system will ignore them completely, so you can write
whatever you want on those lines to explain what things mean. Here's another
version of the configuration above, but this one includes comments that explain
the syntax. You can remove those lines from your configuration file.

```
# The "frequency" attribute controls the FM radio frequency on which the system
# broadcasts audio.
frequency: 101.7 Mhz

# The "programming" attribute stores one or more programs in a list. Notice how
# each line after this is indented by two spaces. Also notice how some lines
# include a dash character. Each dash starts a new program, so this
# configuration describes a total of three programs. When the Magic Radio runs,
# it will inspect each program in order and play audio according to the first
# program which matches the current time.
programming:

  # The following program has a single "start" time that occurs once every
  # week. The program lasts for the duration of 2 selections. The selection may
  # be any file in a folder named "Gunsmoke", which itself is in a folder named
  # "talk". In plain English, this program instructs the Magic Radio to play
  # two episodes of "Gunsmoke" every Wednesday at 6:30 PM.
  - audio: talk/Gunsmoke
    start: Wednesday at 18:30
    duration: 2 selections

  # The following program has multiple "start" times that each occur once every
  # week. The program always lasts for exactly 1 hour. During this time, it
  # will select from any song in the folder named "90s-rock". In plain English,
  # this program instructs the Magic Radio to play rock from the 1990s for an
  # hour on every weekday at 8:00 PM.
  - audio: 90s-rock
    start:
    - Monday at 8:00 PM
    - Tuesday at 8:00 PM
    - Wednesday at 8:00 PM
    - Thursday at 8:00 PM
    - Friday at 8:00 PM
    duration: 1 hour

  # The following program has a single "start" time that occurs just once every
  # year. The program lasts for the duration of one audio selection. That
  # selection is a single file. In plain English, this program instructs the
  # Magic Radio to play the song "Black Coffee" once every year at 4:00 PM on
  # April 25.
  - audio: singles/Everlast - Black Coffee.mp3
    start: April 25 at 4:00 PM
    duration: 1 selection
```

## History files

Programs whose "duration" property is defined in terms of a number of
selections (e.g. `duration: 1 selection` or `duration: 4 selections`) will
choose audio files to play based on the set of available files and a list of
which files have been played in the past. This list of previously-played files
is called the program's "history", and it's stored in a text file named
`magic-radio-history.txt` located in the same directory as the program's audio
files.

The history file has zero or more lines of text. Each line has two parts
separated by a space: the date and time a file was played, and the name of the
file played. Here's an example:

```
2021-01-14T22:00:00 001_Murder_is_a_Knock-Out.mp3
2021-01-18T21:00:00 002_The_Case_of_the_Quarrelsome_Quartet.mp3
```

You can edit these files to influence how the Magic Radio selects audio files
for "selection"-based programs.

If you would like the Magic Radio to "start fresh" and select audio files as
though it has never played any of them before, you may remove this file. The
Magic Radio will automatically start fresh by deleting this file once it's
played every available file for a given program.

If you would like the Magic Radio to replay a recently-played file (for
instance, if you missed an episode of your favorite podcast), you can remove
the line describing that file.

Any other kind of modification to the history files is not supported.

<!--END_INJECTED_INSTRUCTIONS-->

## Architecture

This is a technical summary of how the system continuously plays music
according to the user-defined configuration file.

The system runs the Python program defined in this project a regular intervals:
whenever the system starts up (via `systemd`), whenever an external storage
device is connected (via `usbmount`), and every minute during normal operation
(also via `systemd`).

Whenever the Python application runs, it reads the configuration file to
creates a "playlist," which is a list of one or more songs to play. It does
this by stepping through each "program" in the configuration file until it
finds a program which matches the current time. This looks a little different
for each of the two kinds of programs:

- "duration"-based programs (e.g. "play Tom Waits for 3 hours on Saturdays at
  1:00 PM") - Determine if the current time is within the program's static
  duration.
- "selection" based programs (e.g. "play 2 episodes of [*The
  Shadow*](https://archive.org/details/the-shadow_20200727) at 8:00 PM") -
  First, determine the duration of the program by selecting the audio files
  that are to be played (referencing the program's "history" file if present)
  and calculating the duration of those files (via `ffmpeg`). Then, determine
  if the current time is within that computed duration.

The script then queries Music Player Daemon to determine if audio is currently
playing. It uses the currently-playing song (if any) along with the computed
playlist (if any) to determine what action to take:

- If audio is not playing and a playlist was computed, it loads the computed
  playlist, update's the associated program's "history" file, and initiates
  playback.
- If audio is not playing and no playlist was computed, it exits without
  performing any action.
- If audio is playing and a playlist was computed, it determines if the
  currently-playing audio is included in the computed playlist. If so, then it
  exits without performing any action. If not, then it stops audio playback,
  loads the computed playlist, updates the associated program's "history" file,
  and re-initiates playback.
- If audio is playing, but no active playlist is found, it stops audio
  playback.

## Dependencies

type     | name                                                                    | version | purpose
---------|-------------------------------------------------------------------------|---------|--------
hardware | [Raspberry Pi](https://www.raspberrypi.org/)                            | 4       | run the application code and the associated software
hardware | [DS3231 real-time clock](https://www.adafruit.com/product/4282)         | n/a     | keep accurate time even when the system is powered down
hardware | [Adafruit Stereo FM Transmitter](https://www.adafruit.com/product/1958) | n/a     | broadcast an FM signal to a stock FM radio
software | [Python](https://www.python.org/)                                       | 3.7.0   | interpret configuration files and integrate all other components
software | [Music Player Daemon](https://www.musicpd.org/)                         | 0.20.18 | play audio files
software | [ffmpeg](https://ffmpeg.org/)                                           | 3.4.8   | determine the duration of audio files
software | [systemd](https://systemd.io/)                                          | 241     | scheduling and logging
software | [usbmount](https://github.com/nicokaiser/usbmount)                      | 0.0.24  | storage device mounting

The installation procedure documented below automatically satisfies all
software dependencies.

## Design principles

Most generally, this project has been designed to be maximally simple. It
should only be just complicated enough to offer the desired functionality. This
goal motivates a few more specific principles.

**Stateless is better than stateful.** As a long-lived program runs, it tends
to accrue state. This makes it increasingly difficult to predict its behavior
and to understand when and why things go wrong. It's also harder to write
exhaustive automated tests for all the conditions that might arise over the
course of such a program's operation. This project takes a (mostly) stateless
approach: performing a single service based on a small amount of well-defined
input. That makes it easier to debug and easier to test.

**Behavior should be controlled by text files.** In the grand tradition of UNIX
programs everywhere, the Magic Radio uses human-readable and human-modifiable
text files to decide what to do. This principle empowers people without
technical backgrounds to control the operation of their devices. Both the audio
program configuration and each program's "history" are maintained as
human-readable and human-modifiable text files.

**Configuration should be declarative.** Imperative configuration isn't
configuration at all; it's code. Not only is it more difficult to document and
software that accepts code as input, it's also harder for non-technical folks
to express their intentions.

The precedence-based selection heuristic allows the configuration file to
describe nuanced conditions despite being declarative. For instance:

- "Play '[Auld Lang
  Syne](https://archive.org/details/AuldLangSyneByThomasReid)' at midnight on
  January 1st, regardless of what else may have been scheduled for that time." -
  Create a "selection"-based program for "January 1 at 12:00 AM", and place it
  at the top of the programming list.
- "Play one episode of [*Our Miss
  Brooks*](https://archive.org/details/Our_Miss_Brooks_190_Episodes) on
  Tuesdays at 6:00 PM, and play [old-timey
  commercials](https://archive.org/details/RadioCommercialsShows/) whenever
  it's over." Create one "selection"-based program for "Tuesdays at 6:00 PM".
  Create a "duration"-based program for the same time, and place it immediately
  after the "selection"-based program.

## Installation

Follow these instructions to install the project on a Raspberry Pi 4. In
addition to the hardware listed in the "Dependencies" section, you'll need the
following equipment:

- a USB flash storage drive
- a computer running GNU/Linux (referred to below as the "development machine")
  with `bash, `git`, `tar`, `scp`, and `ssh` installed
- an Ethernet cable

1. [Configure the development machine to share its wired Ethernet
   connection.](https://askubuntu.com/questions/1039907/how-to-share-wired-network-connection-in-18-04)

2. Connect the flash drive to the development machine and find its device
   according to the capacity of all devices connected to your system. In the
   example below, the flash drive can store 16 gigabytes, so its device is
   `sdb`.

       $ lsblk
       NAME              MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
       sda                 8:0    0 238.5G  0 disk
       ├─sda1              8:1    0   512M  0 part  /boot/efi
       ├─sda2              8:2    0   732M  0 part  /boot
       └─sda3              8:3    0 237.3G  0 part
         └─sda3_crypt    253:0    0 237.3G  0 crypt
           ├─system-root 253:1    0    30G  0 lvm   /
           └─system-home 253:2    0 207.3G  0 lvm   /home
       sdb                 8:16   1  14.9G  0 disk
       ├─sdb1              8:17   1   256M  0 part  /media/mike/boot
       └─sdb2              8:18   1  14.6G  0 part  /media/mike/rootfs

3. Download and extract a disk image for the Raspberry Pi OS:

       $ wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2020-12-04/2020-12-02-raspios-buster-armhf-lite.zip
       $ unzip 2020-12-02-raspios-buster-armhf-lite.zip

4. Flash the image to the drive, replacing `sdb` with the appropriate name:

       $ dd bs=4M if=2020-12-02-raspios-buster-armhf-lite.img of=/dev/sdb conv=fsync
       443+0 records in
       443+0 records out
       1858076672 bytes (1.9 GB, 1.7 GiB) copied, 180.705 s, 10.3 MB/s

5. Enable SSH access:

       $ sudo mkdir /mnt/boot
       $ sudo mount /dev/sdb1 /mnt/boot
       $ sudo touch /mnt/boot/ssh
       $ sudo umount /mnt/boot
       $ sudo rmdir /mnt/boot/

6. Remove the flash drive from the development machine, connect it to the
   Raspberry Pi, connect the development machine to the Raspberry Pi using the
   Ethernet cable, and power up the Raspberry Pi

7. Run the installation script:

       $ make install

If the installation script completes successfully, it will print a message
saying so. In that case, your Raspberry Pi is now a Magic Radio!

(I'd like to automate this. [Packer and QEMU seem to be up to the
task](https://linuxhit.com/build-a-raspberry-pi-image-packer-packer-builder-arm/),
but as of January 2021, I haven't been able to install this project's system
dependencies in that environment.)

## Alternate setups

This project was designed and written by one person with just one real-world
application in mind, so it isn't as generic as it could be. Because it is a
free software project, folks are welcome to use this as-is or modify it to suit
their own needs.

For instance, you may want to...

**Remove the Stereo FM transmitter.** The transmitter is the most aesthetic
part of this project: it's what makes it possible to use a stock FM radio
directly. You might prefer to build a device that connects directly to a set of
speakers or a home theater system. In that case, you can leave this device out
and delete the Python code in this project which interfaces with it.

**Install on another single-board computer.** Although the Raspberry Pi 4 is an
excellent device for prototyping embedded systems, this project doesn't depend
on anything unique to that product. If you have another GNU/Linux-ready
single-board computer, that ought to work fine--you'll just have to alter the
installation procedure according to the distribution of GNU/Linux available
there.

**Deploy outside of the east coast of North America.** This project works the
same anywhere in the world, and that's actually a bit of a problem. It
interprets the times in the configuration file in terms of the United State's
Eastern Standard time zone. You'll have to modify the installation script if
you want the times to make sense in some other time zone.

## Potential Improvements

**Increase efficiency through tighter integration with `systemd`.** Inspecting
the configuration file and calculating the current playlist is computationally
expensive. Doing this at the beginning of every minute of operation is pretty
wasteful because for the majority of the time, no action is necessary. A more
intelligent system would only attempt to calculate and apply playlists when
they could be expected to change: at the start and end of every program.
`systemd` is certainly capable of this, but the integration is non-trivial.
Essentially, the system would need to reconfigure `systemd` (installing new
"timer" units and removing outdated ones) at startup and at device-mount-time.

**Make the time zone configurable.** While the time zone can be modified when
the software is installed, it really ought to be configurable by an end user,
via the configuration file. That will allow users to travel with their device
and correct its behavior to suit their new location.

**Memoize access to audio program history files.** The system opens and reads
each audio program's history file every time that program becomes a candidate
for playback. This occurs with great regularity, so it is a source of long-term
wear on the storage device. Because the history files rarely change, the wear
is hardly justified. This project includes a memoization utility to reduce the
inefficiency of similar operations. Unlike the other operations, the system
must be able to selectively invalidate the result of reading history files
(because it changes their contents during routine operation). Although this is
conceptually straightforward, it is challenging to cleanly organize the
necessary code.

## License

Copyright 2020 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
