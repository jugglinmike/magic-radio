from datetime import datetime
import os.path

import pytest

import errors
from program import Program
from parse_configuration import Configuration, StartItem
from playlist import Playlist

PROGRAM_SOURCE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'fixtures',
    'program-source'
)

all_files=(
    os.path.join(PROGRAM_SOURCE, '1-of-3_3-seconds.ogg'),
    os.path.join(PROGRAM_SOURCE, '2-of-3_2-seconds.ogg'),
    os.path.join(PROGRAM_SOURCE, '3-of-3_4-seconds.ogg'),
)

def fake_memoize(fn):
    return fn

@pytest.mark.parametrize('day, time', [
    ((2020, 12, 5), (13, 19)), # Finished one minute ago
    ((2020, 12, 5), (13, 25)), # Starts in one minute
    ((2020, 12, 4), (13, 20)), # Was playing at this time yesterday
    ((2020, 12, 6), (13, 20)), # Will be playing at this time tomorrow
    ((2019, 12, 5), (13, 20)), # Was playing at this time last year
    ((2021, 12, 5), (13, 20)), # Will be playing at this time next year
])
def test_inactive_once_fixed_duration(day, time):
    program = Program(
        start=[StartItem(repetition='once', day=day, time=time)],
        duration=(5, 'minute'),
        audio='',
    )
    at_time = datetime(2020, 12, 5, 13, 24)

    assert program.playlist(at_time, fake_memoize) == None

def test_active_once_fixed_duration():
    program = Program(
        start=[StartItem(repetition='once', day=(2020, 12, 4), time=(13, 20))],
        duration=(5, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 4, 13, 24)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

def test_active_once_fixed_duration_straddle():
    '''Program starts on 2020-12-31 and continues to the next year'''
    program = Program(
        start=[StartItem(repetition='once', day=(2020, 12, 31), time=(23, 50))],
        duration=(20, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2021, 1, 1, 0, 5)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

@pytest.mark.parametrize('day, time', [
    (5, (13, 19)), # Finished one minute ago
    (5, (13, 25)), # Starts in one minute
    (4, (13, 20)), # Was playing at this time yesterday
    (6, (13, 20)), # Will be playing at this time tomorrow
])
def test_inactive_weekly_fixed_duration(day, time):
    program = Program(
        start=[StartItem(repetition='weekly', day=day, time=time)],
        duration=(5, 'minute'),
        audio='',
    )
    at_time = datetime(2020, 12, 4, 13, 24)

    assert program.playlist(at_time, fake_memoize) == None

def test_active_weekly_fixed_duration():
    program = Program(
        start=[StartItem(repetition='weekly', day=5, time=(13, 20))],
        duration=(5, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 4, 13, 24)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

def test_active_weekly_fixed_duration_straddle():
    '''Program starts on Saturdays and continues into the next week'''
    program = Program(
        start=[StartItem(repetition='weekly', day=6, time=(23, 50))],
        duration=(20, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 13, 0, 5)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

def test_active_weekly_fixed_duration_incredibly_long():
    '''Program starts on Wednesday nights and continues until the afternoon of
    the following Wednesday'''
    program = Program(
        start=[StartItem(repetition='weekly', day=3, time=(20, 0))],
        duration=(7 * 24 * 60 - 60, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 16, 7, 30)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

@pytest.mark.parametrize('day, time', [
    ((12, 4), (13, 19)), # Finished one minute ago
    ((12, 4), (13, 25)), # Starts in one minute
    ((12, 3), (13, 20)), # Was playing at this time yesterday
    ((12, 5), (13, 20)), # Will be playing at this time tomorrow
])
def test_inactive_yearly_fixed_duration(day, time):
    program = Program(
        start=[StartItem(repetition='yearly', day=day, time=time)],
        duration=(5, 'minute'),
        audio='',
    )
    at_time = datetime(2020, 12, 4, 13, 24)

    assert program.playlist(at_time, fake_memoize) == None

def test_active_yearly_fixed_duration():
    program = Program(
        start=[StartItem(repetition='yearly', day=(12, 4), time=(13, 20))],
        duration=(5, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 4, 13, 24)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

def test_active_yearly_fixed_duration_incredibly_long():
    '''Program starts on new Year's Eve and continues until the afternoon of
    the following New Year's Eve. This is ridiculous.'''
    program = Program(
        start=[StartItem(repetition='yearly', day=(12, 31), time=(21, 30))],
        duration=(365 * 24 * 60 - 60, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 31, 7, 30)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

def test_active_yearly_fixed_duration_straddle():
    '''Program starts on 2020-12-31 and continues to the next year'''
    program = Program(
        start=[StartItem(repetition='yearly', day=(12, 31), time=(23, 50))],
        duration=(20, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2021, 1, 1, 0, 5)
    expected = Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected

@pytest.mark.parametrize('start_names, is_active', [
    (('once_inactive', 'weekly_inactive', 'yearly_inactive'), False),
    (('once_active', 'weekly_inactive', 'yearly_inactive'), True),
    (('weekly_inactive', 'once_active', 'yearly_inactive'), True),
    (('weekly_inactive', 'yearly_inactive', 'once_active'), True),
    (('weekly_active', 'once_inactive', 'yearly_inactive'), True),
    (('once_inactive', 'weekly_active', 'yearly_inactive'), True),
    (('once_inactive', 'yearly_inactive', 'weekly_active'), True),
    (('yearly_active', 'once_inactive', 'weekly_inactive'), True),
    (('once_inactive', 'yearly_active', 'weekly_inactive'), True),
    (('once_inactive', 'weekly_inactive', 'yearly_active'), True),
])
def test_start_time_prioritization(start_names, is_active):
    start_items = dict(
        once_active=StartItem(repetition='once', day=(2020, 12, 12), time=(18, 38)),
        once_inactive=StartItem(repetition='once', day=(2021, 12, 12), time=(18, 38)),
        weekly_active=StartItem(repetition='weekly', day=6, time=(18, 38)),
        weekly_inactive=StartItem(repetition='weekly', day=5, time=(18, 38)),
        yearly_active=StartItem(repetition='yearly', day=(12, 12), time=(18, 38)),
        yearly_inactive=StartItem(repetition='yearly', day=(12, 13), time=(18, 38)),
    )
    start = []
    for start_name in start_names:
        start.append(start_items[start_name])

    program = Program(
        start=start,
        duration=(20, 'minute'),
        audio=PROGRAM_SOURCE,
    )
    at_time = datetime(2020, 12, 12, 18, 39)

    expected = None if not is_active else Playlist(
        filenames=all_files,
        offset=None,
        shuffle=True,
    )

    assert program.playlist(at_time, fake_memoize) == expected
