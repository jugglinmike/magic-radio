import os.path

import pytest

import errors
from parse_configuration import parse_configuration, StartItem

CWD = os.getcwd()

def create_config(frequency='Null', start='2021-01-01 at 12:00 AM', duration='1 selection', audio='.'):
    return '''
        version: 1
        frequency: {frequency}
        programming:
          - start: wednesday at 06:30 PM
            duration: 1 selection
            audio: .
          - start: {start}
            duration: {duration}
            audio: {audio}
          - start: friday at 06:30 PM
            duration: 1 selection
            audio: .
    '''.format(frequency=frequency, start=start, duration=duration, audio=audio)

def test_invalid_yaml():
    with pytest.raises(Exception):
        parse_configuration('[][]')

def test_missing_version():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
          frequency: Null
          programming:
            - start: wednesday at 06:30 PM
              duration: 1 selection
              audio: .
        ''')

def test_missing_frequency():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
          version: 1
          programming:
            - start: wednesday at 06:30 PM
              duration: 1 selection
              audio: .
        ''')

@pytest.mark.parametrize('frequency', [
    '108.1 Mhz',
    '108.1Mhz',
    '108.1mhz',
    '108.1',
    108.1,
    '87.4 Mhz',
    '87.4Mhz',
    '87.4mhz',
    '87.4',
    87.4,
])
def test_invalid_frequency(frequency):
    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(frequency=frequency))

@pytest.mark.parametrize('raw_frequency,parsed_frequency', [
    ('Null', None),
    ('NULL', None),
    ('102 Mhz', 102000),
    ('102Mhz', 102000),
    ('102mhz', 102000),
    ('102', 102000),
    (102, 102000),
    ('103.1 Mhz', 103100),
    ('103.1Mhz', 103100),
    ('103.1mhz', 103100),
    ('103.1', 103100),
    (103.1, 103100),
    ('92 Mhz', 92000),
    ('92Mhz', 92000),
    ('92mhz', 92000),
    ('92', 92000),
    (92, 92000),
    ('93.1 Mhz', 93100),
    ('93.1Mhz', 93100),
    ('93.1mhz', 93100),
    ('93.1', 93100),
    (93.1, 93100)
])
def test_valid_frequency(raw_frequency, parsed_frequency):
    config = parse_configuration(CWD, create_config(frequency=raw_frequency))

    assert config.frequency == parsed_frequency

def test_missing_programming():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
            version: 1
            frequency: Null
        ''')

def test_missing_start():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
          version: 1
          frequency: Null
          programming:
            - title: first
              start: wednesday at 06:30 PM
              duration: 1 selection
              audio: .
            - title: second
              duration: 1 selection
              audio: .
            - title: third
              start: friday at 06:30 PM
              duration: 1 selection
              audio: .
        ''')

@pytest.mark.parametrize('start', [
    '2021-01-01 12:00 AM',
    '2021-01-01 a 12:00 AM',
    '2021-01-01 @ 12:00 AM',
    '2020-02-30 at 12:00 AM', # never valid
    '2021-02-29 at 12:00 AM', # only valid on leap years
    'february 30 at 12:00 AM',
    'thorsday at 12:00 AM',
    '2021-01-01 at 13:00 AM',
    '2021-01-01 at 12:60 AM',
])
def test_bad_start(start):
    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(start=start))

@pytest.mark.parametrize('start,parsed', [
    ('2020-02-29 at 12:00 PM', (2020, 2, 29, 12, 0)),
    ('2020-02-29 at 12:00 AM', (2020, 2, 29, 0, 0)),
    ('2020-02-29 at 12:00 pm', (2020, 2, 29, 12, 0)),
    ('2020-02-29 at 12:00 am', (2020, 2, 29, 0, 0)),
    ('2020-02-29 at 12:59 PM', (2020, 2, 29, 12, 59)),
    ('2020-02-29 at 01:00 PM', (2020, 2, 29, 13, 0)),
    ('2020-02-29 at 1:00 PM', (2020, 2, 29, 13, 0)),
    ('2021-02-28 at 12:00 PM', (2021, 2, 28, 12, 0)),
])
def test_good_start_once(start, parsed):
    config = parse_configuration(CWD, create_config(start=start))
    expected = [StartItem(
        repetition='once',
        day=parsed[:3],
        time=parsed[3:],
    )]

    assert config.programming[1].start == expected

@pytest.mark.parametrize('start,parsed', [
    ('February 29 at 12:00 PM', (2, 29, 12, 0)),
    ('february 29 at 12:00 PM', (2, 29, 12, 0)),
])
def test_good_start_yearly(start, parsed):
    config = parse_configuration(CWD, create_config(start=start))
    expected = [StartItem(
        repetition='yearly',
        day=parsed[:2],
        time=parsed[2:],
    )]

    assert config.programming[1].start == expected

@pytest.mark.parametrize('start,parsed', [
    ('Sunday at 4:20 PM', (0, 16, 20)),
    ('thursday at 12:00 PM', (4, 12, 0)),
    ('fridays at 8:12 AM', (5, 8, 12)),
])
def test_good_start_weekly(start, parsed):
    config = parse_configuration(CWD, create_config(start=start))
    expected = [StartItem(
        repetition='weekly',
        day=parsed[0],
        time=parsed[1:],
    )]

    assert config.programming[1].start == expected

def test_good_start_mixed():
    start = [
        '2020-02-29 at 12:00 PM',
        'February 29 at 12:00 PM',
        'Sunday at 4:20 PM',
    ]
    config = parse_configuration(CWD, create_config(start=start))
    expected = [
        StartItem(repetition='once', day=(2020, 2, 29), time=(12, 0)),
        StartItem(repetition='yearly', day=(2, 29), time=(12, 0)),
        StartItem(repetition='weekly', day=0, time=(16,20)),
    ]

    assert config.programming[1].start == expected

def test_missing_duration():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
          version: 1
          frequency: Null
          programming:
            - title: first
              start: wednesday at 06:30 PM
              duration: 1 selection
              audio: .
            - title: second
              start: thursday at 06:30 PM
              audio: .
            - title: third
              start: friday at 06:30 PM
              duration: 1 selection
              audio: .
        ''')

@pytest.mark.parametrize('duration', [
    'selection',
    'selections',
    '0 selection',
    '0 selections',
    '1 second',
    '1 seconds',
    '1 day',
    '1 days',
])
def test_bad_duration(duration):
    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(duration=duration))

@pytest.mark.parametrize('duration,parsed', [
    ('1 selection', (1, 'selection')),
    ('1 selections', (1, 'selection')),
    ('7 selection', (7, 'selection')),
    ('7 selections', (7, 'selection')),
    ('14 selection', (14, 'selection')),
    ('14 selections', (14, 'selection')),
    ('1 minute', (1, 'minute')),
    ('1 minutes', (1, 'minute')),
    ('7 minute', (7, 'minute')),
    ('7 minutes', (7, 'minute')),
    ('14 minute', (14, 'minute')),
    ('14 minutes', (14, 'minute')),
    ('1 hour', (60, 'minute')),
    ('1 hours', (60, 'minute')),
    ('7 hour', (420, 'minute')),
    ('7 hours', (420, 'minute')),
    ('14 hour', (840, 'minute')),
    ('14 hours', (840, 'minute')),
])
def test_good_duration(duration, parsed):
    config = parse_configuration(CWD, create_config(duration=duration))

    assert config.programming[1].duration == parsed

def test_missing_audio():
    with pytest.raises(errors.MissingConfigurationError):
        parse_configuration(CWD, '''
          version: 1
          frequency: Null
          programming:
            - title: first
              start: wednesday at 06:30 PM
              duration: 1 selection
              audio: .
            - title: second
              start: thursday at 06:30 PM
              duration: 1 selection
            - title: third
              start: friday at 06:30 PM
              duration: 1 selection
              audio: .
        ''')

@pytest.mark.parametrize('audio', [
    './non-existent-name',
])
def test_bad_audio(audio):
    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(audio=audio))

def test_bad_audio_empty_dir(tmp_dir):
    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(audio=tmp_dir))

def test_bad_audio_empty_dir_with_empty_subdirs(tmp_dir):
    os.makedirs(os.path.join(tmp_dir, 'a', 'b'))
    os.makedirs(os.path.join(tmp_dir, 'c', 'd'))

    with pytest.raises(errors.InvalidConfigurationError):
        parse_configuration(CWD, create_config(audio=tmp_dir))

def test_good_audio_file(tmp_dir):
    file_name = os.path.join(tmp_dir, 'foo')
    open(file_name, 'w')

    config = parse_configuration(CWD, create_config(audio=file_name))

    assert config.programming[1].audio == file_name

def test_good_audio_directory(tmp_dir):
    open(os.path.join(tmp_dir, 'foo'), 'w')

    config = parse_configuration(CWD, create_config(audio=tmp_dir))

    assert config.programming[1].audio == tmp_dir
