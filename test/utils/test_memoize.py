import pytest

from utils.memoize import create_memoizer, clear_cache

class NonTrivialToSerialize(object):
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return type(other) == NonTrivialToSerialize and self.value == other.value

def make_nonidempotent_fn():
    box = dict(value=0)
    def nonidempotent_fn(unused):
        box['value'] += 1
        return box['value']

    return nonidempotent_fn

def test_null_memoizer():
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(None)

    memoized = memoize(nonidempotent_fn)

    assert memoized('typical input') == 1
    assert memoized('typical input') == 2

def test_null_memoizer_clear():
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(None)

    memoized = memoize(nonidempotent_fn)

    assert memoized('typical input') == 1
    assert memoized('typical input') == 2

    clear_cache(None)

    assert memoized('typical input') == 3
    assert memoized('typical input') == 4

@pytest.mark.parametrize('input_value', [
    'generic_filename.txt',
    '/absolute/path',
    '../relative/path',
])
def test_real_memoizer_hit(input_value, tmp_dir):
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(tmp_dir)

    memoized = memoize(nonidempotent_fn)

    assert memoized(input_value) == 1
    assert memoized(input_value) == 1

@pytest.mark.parametrize('result', [
    234,
    'a string',
    None,
    True,
    NonTrivialToSerialize(345),
    NonTrivialToSerialize(NonTrivialToSerialize(567)),
])
def test_real_memoizer_value(result, tmp_dir):
    def tricky(name):
        return result

    memoize = create_memoizer(tmp_dir)
    memoized = memoize(tricky)

    assert memoized('typical input') == result
    assert memoized('typical input') == result

def test_real_memoizer_miss_different_operation(tmp_dir):
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(tmp_dir)

    memoized1 = memoize(nonidempotent_fn)
    nonidempotent_fn.__name__ = 'another_operation'
    memoized2 = memoize(nonidempotent_fn)

    assert memoized1('typical input') == 1
    assert memoized2('typical input') == 2

def test_real_memoizer_miss_different_input(tmp_dir):
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(tmp_dir)

    memoized = memoize(nonidempotent_fn)

    assert memoized('typical input') == 1
    assert memoized('b') == 2

def test_real_memoizer_reuse(tmp_dir):
    '''Simulate condition where cache directory exists, as in repeated
    invocations of the program'''
    nonidempotent_fn = make_nonidempotent_fn()

    memoize1 = create_memoizer(tmp_dir)

    memoized1 = memoize1(nonidempotent_fn)

    assert memoized1('typical input') == 1

    memoize2 = create_memoizer(tmp_dir)

    memoized2 = memoize2(nonidempotent_fn)

    assert memoized2('typical input') == 1

def test_real_memoizer_clear(tmp_dir):
    nonidempotent_fn = make_nonidempotent_fn()

    memoize = create_memoizer(tmp_dir)

    memoized = memoize(nonidempotent_fn)

    assert memoized('typical input') == 1
    assert memoized('typical input') == 1

    clear_cache(tmp_dir)

    assert memoized('typical input') == 2
    assert memoized('typical input') == 2
