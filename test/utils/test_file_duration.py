import pytest

from utils.file_duration import file_duration

@pytest.mark.parametrize('name,expected', [
    ('./test/fixtures/silence-6400.mp3', 6360),
    ('./test/fixtures/silence-2055.wav', 2050),
    ('./test/fixtures/silence-180000.ogg', 180010),
])
def test_valid_file(name, expected):
    assert file_duration(name) == expected

def test_non_existent_file():
    with pytest.raises(FileNotFoundError):
        file_duration('there_is_no_file_with_this_name.mp3')

def test_non_audio_file():
    with pytest.raises(TypeError):
        file_duration(__file__)
