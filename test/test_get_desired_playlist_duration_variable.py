from datetime import datetime
import os.path

import pytest

from program import Program
from parse_configuration import Configuration, StartItem
from playlist import Playlist

PROGRAM_SOURCE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'fixtures',
    'program-source'
)

all_files=(
    '1-of-3_3-seconds.ogg',
    '2-of-3_2-seconds.ogg',
    '3-of-3_4-seconds.ogg',
)

def join_all(prefix, filenames):
    return tuple(os.path.join(prefix, filename) for filename in filenames)

def fake_memoize(fn):
    return fn

@pytest.mark.parametrize('count, day, time', [
    (1, (2020, 12, 5), (13, 21)), # Finished one minute ago (1 selection)
    (2, (2020, 12, 5), (13, 19)), # Finished one minute ago (2 selections)
    (3, (2020, 12, 5), (13, 15)), # Finished one minute ago (3 selections)
    (1, (2020, 12, 5), (13, 25)), # Starts in one minute
    (1, (2020, 12, 4), (13, 23)), # Was playing at this time yesterday
    (1, (2020, 12, 6), (13, 23)), # Will be playing at this time tomorrow
    (1, (2019, 12, 5), (13, 23)), # Was playing at this time last year
    (1, (2021, 12, 5), (13, 23)), # Will be playing at this time next year
])
def test_inactive_once_variable_duration(count, day, time):
    program = Program(
        start=[StartItem(repetition='once', day=day, time=time)],
        duration=(count, 'selection'),
        audio=PROGRAM_SOURCE,
    )

    at_time = datetime(2020, 12, 5, 13, 24)

    assert program.playlist(at_time, fake_memoize) == None

@pytest.mark.parametrize('count, time_spec, filenames, offset', [
    (3, (2020, 12, 4, 13, 23), all_files, 0),
    (3, (2020, 12, 4, 13, 24), all_files, 1),
    (3, (2020, 12, 4, 13, 25), all_files, 2),
    (3, (2020, 12, 4, 13, 26), all_files[1:], 0),
    (3, (2020, 12, 4, 13, 27), all_files[1:], 1),
    (3, (2020, 12, 4, 13, 28), all_files[2:], 0),
    (3, (2020, 12, 4, 13, 29), all_files[2:], 1),
    (3, (2020, 12, 4, 13, 30), all_files[2:], 2),
    (3, (2020, 12, 4, 13, 31), all_files[2:], 3),
    (2, (2020, 12, 4, 13, 23), all_files[:2], 0),
    (2, (2020, 12, 4, 13, 24), all_files[:2], 1),
    (2, (2020, 12, 4, 13, 25), all_files[:2], 2),
    (2, (2020, 12, 4, 13, 26), all_files[1:2], 0),
    (2, (2020, 12, 4, 13, 27), all_files[1:2], 1),
])
def test_active_once_variable_duration(count, time_spec, filenames, offset):
    program = Program(
        start=[StartItem(repetition='once', day=(2020, 12, 4), time=(13, 23))],
        duration=(count, 'selection'),
        audio=PROGRAM_SOURCE,
    )
    expected = Playlist(filenames=join_all(PROGRAM_SOURCE, filenames), offset=offset, shuffle=False)

    assert program.playlist(datetime(*time_spec), fake_memoize) == expected

@pytest.mark.parametrize('count, time_spec, filenames, offset', [
    (3, (2020, 12, 31, 23, 59), all_files, 0),
    (3, (2021, 1, 1, 0, 0), all_files, 1),
    (3, (2021, 1, 1, 0, 1), all_files, 2),
    (3, (2021, 1, 1, 0, 2), all_files[1:], 0),
    (3, (2021, 1, 1, 0, 3), all_files[1:], 1),
    (3, (2021, 1, 1, 0, 4), all_files[2:], 0),
    (3, (2021, 1, 1, 0, 5), all_files[2:], 1),
    (3, (2021, 1, 1, 0, 6), all_files[2:], 2),
    (3, (2021, 1, 1, 0, 7), all_files[2:], 3),
    (2, (2020, 12, 31, 23, 59), all_files[:2], 0),
    (2, (2021, 1, 1, 0, 0), all_files[:2], 1),
    (2, (2021, 1, 1, 0, 1), all_files[:2], 2),
    (2, (2021, 1, 1, 0, 2), all_files[1:2], 0),
    (2, (2021, 1, 1, 0, 3), all_files[1:2], 1),
])
def test_active_weekly_variable_duration_straddle(count, time_spec, filenames, offset):
    '''Program starts on 2020-12-31 and continues to the next year'''
    program = Program(
        start=[StartItem(repetition='once', day=(2020, 12, 31), time=(23, 59))],
        duration=(count, 'selection'),
        audio=PROGRAM_SOURCE,
    )
    expected = Playlist(filenames=join_all(PROGRAM_SOURCE, filenames), offset=offset, shuffle=False)

    assert program.playlist(datetime(*time_spec), fake_memoize) == expected

@pytest.mark.parametrize('count, history, filenames, offset, history_cleared', [
    (3, (), all_files, 2, False), # Tolerate empty history files
    (3, ('something else',), all_files, 2, False),
    (3, all_files[:1], all_files[2:], 0, False),
    (3, all_files[1:2], (all_files[0], all_files[2]), 2, False),
    (3, all_files[2:], all_files[:2], 2, False),
    (3, (all_files[0], all_files[1]), all_files[2:], 2, False),
    (3, (all_files[1], all_files[0]), all_files[2:], 2, False),
    (3, (all_files[0], all_files[2]), (), None, False),
    (3, (all_files[2], all_files[0]), (), None, False),
    (3, (all_files[1], all_files[2]), all_files[:1], 2, False),
    (3, (all_files[2], all_files[1]), all_files[:1], 2, False),

    # The history is "full." Clear it and restart.
    (3, (all_files[0], all_files[1], all_files[2]), all_files, 2, True),
    (3, (all_files[0], all_files[2], all_files[1]), all_files, 2, True),
    (3, (all_files[1], all_files[0], all_files[2]), all_files, 2, True),
    (3, (all_files[1], all_files[2], all_files[0]), all_files, 2, True),
    (3, (all_files[2], all_files[0], all_files[1]), all_files, 2, True),
    (3, (all_files[2], all_files[1], all_files[0]), all_files, 2, True),

    (2, all_files[:1], all_files[2:], 0, False),
    (2, all_files[1:2], (all_files[0], all_files[2]), 2, False),
    (2, all_files[2:], all_files[:2], 2, False),
    (2, (all_files[0], all_files[1]), all_files[2:], 2, False),
    (2, (all_files[1], all_files[0]), all_files[2:], 2, False),
    (2, (all_files[0], all_files[2]), (), None, False),
    (2, (all_files[2], all_files[0]), (), None, False),
    (2, (all_files[1], all_files[2]), all_files[:1], 2, False),
    (2, (all_files[2], all_files[1]), all_files[:1], 2, False),

    # The history is "full." Clear it and restart.
    (2, (all_files[0], all_files[1], all_files[2]), all_files[:2], 2, True),
    (2, (all_files[0], all_files[2], all_files[1]), all_files[:2], 2, True),
    (2, (all_files[1], all_files[0], all_files[2]), all_files[:2], 2, True),
    (2, (all_files[1], all_files[2], all_files[0]), all_files[:2], 2, True),
    (2, (all_files[2], all_files[0], all_files[1]), all_files[:2], 2, True),
    (2, (all_files[2], all_files[1], all_files[0]), all_files[:2], 2, True),

    (1, all_files[:1], (), None, False),
    (1, all_files[1:2], all_files[:1], 2, False),
    (1, all_files[2:], all_files[:1], 2, False),
    (1, (all_files[0], all_files[1]), all_files[2:], 2, False),
    (1, (all_files[1], all_files[0]), all_files[2:], 2, False),
    (1, (all_files[0], all_files[2]), (), None, False),
    (1, (all_files[2], all_files[0]), (), None, False),
    (1, (all_files[1], all_files[2]), all_files[:1], 2, False),
    (1, (all_files[2], all_files[1]), all_files[:1], 2, False),

    # The history is "full." Clear it and restart.
    (1, (all_files[0], all_files[1], all_files[2]), all_files[:1], 2, True),
    (1, (all_files[0], all_files[2], all_files[1]), all_files[:1], 2, True),
    (1, (all_files[1], all_files[0], all_files[2]), all_files[:1], 2, True),
    (1, (all_files[1], all_files[2], all_files[0]), all_files[:1], 2, True),
    (1, (all_files[2], all_files[0], all_files[1]), all_files[:1], 2, True),
    (1, (all_files[2], all_files[1], all_files[0]), all_files[:1], 2, True),
])
def test_history_influence(count, history, filenames, offset, history_cleared, open_temp_lax):
    program = Program(
        start=[
            StartItem(repetition='once', day=(2020, 12, 12), time=(19, 49))
        ],
        duration=(count, 'selection'),
        audio=PROGRAM_SOURCE,
    )
    history_filename = os.path.join(PROGRAM_SOURCE, 'magic-radio-history.txt')
    history_contents = '\n'.join([
        '2020-12-01T00:00:00 {}'.format(filename) for filename in history
    ])
    with open_temp_lax(history_filename, 'w') as handle:
        handle.write(history_contents)
    at_time = datetime(2020, 12, 12, 19, 51)
    expected = None if offset == None else Playlist(filenames=join_all(PROGRAM_SOURCE, filenames), offset=offset, shuffle=False)

    assert program.playlist(at_time, fake_memoize) == expected
    assert os.path.isfile(history_filename) != history_cleared

@pytest.mark.parametrize('entry_time, current_minute, expected_filenames, offset', [
    # the history entry is from a previous instance of the program, so it's
    # assumed to have been completed, and the file is *not* included in the
    # playlist:
    ('2020-12-12T19:48:59', 50, all_files[1:], 1),
    # the history entry is from a the current program, so the file is included
    # in the playlist:
    ('2020-12-12T19:49:00', 50, all_files[:2], 1),
    # the history entry is from a the current program, but we're far enough
    # from the program's start to know that playback for this particular file
    # must have completed, so the file is *not* included in the playlist
    ('2020-12-12T19:49:00', 52, all_files[1:2], 0),
])
def test_history_interrupted(entry_time, current_minute, expected_filenames, offset, open_temp):
    program = Program(
        start=[
            StartItem(repetition='once', day=(2020, 12, 12), time=(19, 49))
        ],
        duration=(2, 'selection'),
        audio=PROGRAM_SOURCE,
    )
    history_filename = os.path.join(PROGRAM_SOURCE, 'magic-radio-history.txt')
    history_contents = '\n'.join([
        '{} {}'.format(entry_time, all_files[0]),
    ])
    with open_temp(history_filename, 'w') as handle:
        handle.write(history_contents)
    at_time = datetime(2020, 12, 12, 19, current_minute)
    expected = Playlist(filenames=join_all(PROGRAM_SOURCE, expected_filenames), offset=offset, shuffle=False)

    assert program.playlist(at_time, fake_memoize) == expected
