import os.path
import sys

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '..',
        'src'
    )
)

from contextlib import contextmanager
import os
import shutil
import tempfile

import pytest

@pytest.fixture()
def tmp_dir():
    name = tempfile.mkdtemp(prefix='magic_radio_')
    yield name
    shutil.rmtree(name)

@pytest.fixture()
def open_temp():
    '''Provide a function that will open a file and return its handle and that
    will destroy the file when the test completes.'''
    names = []

    @contextmanager
    def fn(name, *args, **kwargs):
        names.append(name)
        with open(name, *args, **kwargs) as handle:
            yield handle

    yield fn

    for name in names:
        os.remove(name)

@pytest.fixture()
def open_temp_lax():
    '''Provide a function that will open a file and return its handle and that
    will destroy the file when the test completes. Unlike `open_temp`, this
    fixture tolerates cases where the file has already been destroyed.'''
    names = []

    @contextmanager
    def fn(name, *args, **kwargs):
        names.append(name)
        with open(name, *args, **kwargs) as handle:
            yield handle

    yield fn

    for name in names:
        try:
            os.remove(name)
        except FileNotFoundError:
            pass
