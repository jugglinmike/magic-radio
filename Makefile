.PHONY: test
test:
	grep --quiet '<!--BEGIN_INJECTED_INSTRUCTIONS-->' README.md
	grep --quiet '<!--END_INJECTED_INSTRUCTIONS-->' README.md
	pytest

.PHONY: install
install:
	./scripts/install.sh
