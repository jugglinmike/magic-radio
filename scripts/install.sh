#!/bin/bash

set -exu

ip=''

# Wait for the Raspberry Pi's IP address to be available
while true ; do
  ip=$(cat /var/lib/misc/dnsmasq.leases | awk '/raspberrypi/{print $3}')

  if [ ! -z "$ip" ]; then
    break
  fi

  sleep 1
done

# Wait for the SSH server to be ready
while ! nc -zw3 $ip 22 ; do
  sleep 1
done

source_code_archive=magic-radio-source.tar
script_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

tar cvf $source_code_archive $(git ls-files)

function cleanup {
  rm $source_code_archive
}
trap cleanup EXIT

scp $source_code_archive pi@$ip:.

ssh pi@$ip \
  "sudo SOURCE_CODE_ARCHIVE=${source_code_archive} bash -s" \
  < $script_directory/provision.sh

set +x
