#!/bin/bash

set -exu

# Update list of system packages
apt-get update

# Install required system packages
apt-get install --yes ffmpeg mpd logrotate python3-pip

# Install usbmount to dynamically mount whatever's plugged into the USB port to
# /media/usb0
curl --location https://github.com/nicokaiser/usbmount/releases/download/0.0.24/usbmount_0.0.24_all.deb --output usbmount_0.0.24_all.deb
apt install --yes --fix-broken ./usbmount_0.0.24_all.deb

# Set the time zone for use in Boston
timedatectl set-timezone America/New_York

# Configure Music Player Daemon to scan the USB drive (as mounted by the
# usbmount package)
sed -i 's:music_directory.*$:music_directory "/media/usb0":' \
  /etc/mpd.conf

app_user=pi
log_file='/media/usb0/magic-radio.log'
cache_dir='/run/magic-radio'
app_dir="/home/${app_user}/magic-radio"
run_command="/usr/bin/python3 ${app_dir}/src/main.py \
  --cache-dir $cache_dir \
  /media/usb0"

# Install required Python packages
mkdir -p $app_dir
pushd $app_dir
tar xvf ../$SOURCE_CODE_ARCHIVE
pip3 install -r requirements.txt
popd
rm $SOURCE_CODE_ARCHIVE
chown -R $app_user:$app_user $app_dir

# Create a cache directory on the in-memory file system to limit wear on the
# storage device
mkdir -p $cache_dir
chown $app_user:$app_user $cache_dir
# Configure the system to re-create this directory automatically and to clear
# it once a day.
echo "
#Type Path        Mode User      Group      Age Argument
d     $cache_dir  0755 $app_user $app_user  1d  -
" > /etc/tmpfiles.d/magic-radio.conf

# Configure log rotation to avoid filling the external drive with logging data
echo "
$log_file {
  notifempty
  rotate 10
  size 100k
  olddir
  createolddir 744 $app_user $app_user
}
" > /etc/logrotate.d/magic-radio

echo "
[Unit]
Description=Plays audio according to a schedule
After=mpd.service
Wants=magic-radio.timer

[Service]
Type=oneshot
ExecStart=$run_command
User=$app_user
Group=$app_user
StandardOutput=append:$log_file
StandardError=append:$log_file
" > /etc/systemd/system/magic-radio.service

echo "
[Unit]
Description=Plays audio according to a schedule, rescanning the audio library
Requires=magic-radio.service

[Timer]
Unit=magic-radio.service
# Execute the service every minute
OnCalendar=*-*-* *:*:00
# Require 1-second accuracy
AccuracySec=1s

[Install]
WantedBy=timers.target
" > /etc/systemd/system/magic-radio.timer

# Create a systemd service that rescans the audio library, scheduled to run
# whenever the system starts. The user may add or remove audio files while the
# system is powered down, and this service ensures the system recognizes any
# such changes. (The user may also make changes by removing and re-inserting
# the audio storage device without restarting; this service is referenced by
# the `usbmount` utility to detect changes in that case.)
echo "
[Unit]
Description=Logs system statistics to the systemd journal
After=mpd.service

[Service]
Type=oneshot
ExecStart=$run_command --rescan
User=$app_user
Group=$app_user
StandardOutput=append:$log_file
StandardError=append:$log_file

[Install]
# Run this service when the system starts
WantedBy=multi-user.target
" > /etc/systemd/system/magic-radio-rescan.service

systemctl daemon-reload
systemctl enable magic-radio-rescan.service
systemctl enable --now magic-radio.timer

# Configure usbmount to rescan the MPD database whenever the USB drive is
# mounted.
mount_script=/etc/usbmount/mount.d/01_magic_radio_rescan
echo "#!/bin/sh
systemctl start magic-radio-rescan.service
" > $mount_script
chmod +x $mount_script

# Configure usbmount to set the owner of all files to the application user so
# that it can write history files. (This only applies to vfat-formatted USB
# drives, so it would need to be changed to support others.)
sed -i "s/^FS_MOUNTOPTIONS.*$/FS_MOUNTOPTIONS=\"-fstype=vfat,uid=${app_user}\"/" \
  /etc/usbmount/usbmount.conf

# Maximize volume
amixer sset Headphone 100%

# Install FM-tuner-related dependencies
apt-get install -y python-smbus i2c-tools
# The following commands are undocumented, so they may not work with future
# builds of RaspOS. In that case, I2C and SPI should be enabled manually
# https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
# https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-spi
# Enable I2C
raspi-config nonint do_i2c 0
# Enable SPI
raspi-config nonint do_spi 0

# Replace fake hardware clock with authentic hardware clock
overlay_desired='dtoverlay=i2c-rtc,ds3231'
overlay_pattern='^dtoverlay=i2c-rtc,.*'
if grep --quiet -E $overlay_pattern /boot/config.txt ; then
  sed -i "s/$overlay_pattern/$overlay_desired/" /boot/config.txt
else
  echo $overlay_desired >> /boot/config.txt
fi
apt-get -y remove fake-hwclock
update-rc.d -f fake-hwclock remove
systemctl disable fake-hwclock
cat > /lib/udev/hwclock-set <<'CLOCK'
#!/bin/sh
# Reset the System Clock to UTC if the hardware clock from which it
# was copied by the kernel was in localtime.

dev=$1

# Disabled because a hardware clock has been installed for this system.
#if [ -e /run/systemd/system ] ; then
#    exit 0
#fi

if [ -e /run/udev/hwclock-set ]; then
    exit 0
fi

if [ -f /etc/default/rcS ] ; then
    . /etc/default/rcS
fi

# These defaults are user-overridable in /etc/default/hwclock
BADYEAR=no
HWCLOCKACCESS=yes
HWCLOCKPARS=
HCTOSYS_DEVICE=rtc0
if [ -f /etc/default/hwclock ] ; then
    . /etc/default/hwclock
fi

if [ yes = "$BADYEAR" ] ; then
    # Disabled because a hardware clock has been installed for this system.
    #/sbin/hwclock --rtc=$dev --systz --badyear
    /sbin/hwclock --rtc=$dev --hctosys --badyear
else
    # Disabled because a hardware clock has been installed for this system.
    #/sbin/hwclock --rtc=$dev --systz
    /sbin/hwclock --rtc=$dev --hctosys
fi

# Note 'touch' may not be available in initramfs
> /run/udev/hwclock-set
CLOCK

set +x

echo '
*******************************************************************************
****                       Installation successful                         ****
*******************************************************************************

If you are reading this, then you have successfully installed the Magic Radio
project. Congrats!

Now rebooting to finalize the installation...
'

# Fancy reboot command which ensures a successful and race-free exit. Rebooting
# is necessary to complete the enabling of I2C (just this once, though; the
# system will be resilient to subsequent unexpected power outages).
nohup reboot > /dev/null 2>&1 & exit
