# Configuration File Specification

```yaml
version: VERSION
frequency: FREQUENCY
programming:
  - PROGRAM
```

### Program

- start: a single start time specification or a list of start time
  specifications
- duration: time specification or "x selection(s)"
- audio: a filesystem path to a file a directory

```
frequency: 101.7 Mhz
programming:
  - start: wednesday at 18:30
    duration: 1 selection
    audio: talk/The Shadow
  - start:
    - sunday at 20:00
    - monday at 20:00
    - tuesday at 20:00
    - wednesday at 20:00
    - thursday at 20:00
    - friday at 20:00
    - saturday at 20:00
    duration: 1 hour
    audio: 90s
  - start: april 25 at 6:00 PM
    duration: 1 selection
    audio: singles/Everlast - Black Coffee.mp3
  - start: january 1 at 12:00 AM
    duration: 1 selection
    audio: singles/Guy Lombardo - Auld Lang Syne.mp3
```

### Start specification

- `START`: `DAY_SPEC` "at" `TIME_OF_DAY`
- `DAY_SPEC`:
  - `DATE`
  - `DAY_OF_WEEK`
  - `DAY_OF_MONTH`
- `DATE`: `YYYY-MM-DD`
- `DAY_OF_WEEK`:
  - `"sunday"`
  - `"monday"`
  - `"tuesday"`
  - `"wednesday"`
  - `"thursday"`
  - `"friday"`
  - `"saturday"`
- `DAY_OF_MONTH`
  - "january" `ONE_TO_THIRTY_ONE`
  - "february" `ONE_TO_TWENTY_NINE`
  - "march" `ONE_TO_THIRTY_ONE`
  - "april" `ONE_TO_THIRTY`
  - "may" `ONE_TO_THIRTY_ONE`
  - "june" `ONE_TO_THIRTY`
  - "july" `ONE_TO_THIRTY_ONE`
  - "august" `ONE_TO_THIRTY_ONE`
  - "september" `ONE_TO_THIRTY`
  - "october" `ONE_TO_THIRTY_ONE`
  - "november" `ONE_TO_THIRTY`
  - "december" `ONE_TO_THIRTY_ONE`
- `TIME_OF_DAY`: `HH:MM`

## Validation tasks

- Configuration syntax
  - Structure
  - Duration specifications
  - Start time specifications
- audio file/directory existence
